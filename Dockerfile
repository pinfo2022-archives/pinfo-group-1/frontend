FROM node:lts-alpine as build
ARG NG_CONF=production
WORKDIR /src
COPY package.json package-lock.json ./
RUN npm install
COPY . ./
RUN npm run build -- --configuration=${NG_CONF}

FROM nginx:stable-alpine
COPY --from=build /src/dist /usr/share/nginx/html
EXPOSE 80
