declare var process: {
  env: {
    NG_APP_ENV: string;
    OAUTH2_CLIENT_ID: string
    OAUTH2_DOMAIN: string
    API_URL: string
    
    [key: string]: any;
  };
};
