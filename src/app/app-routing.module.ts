import { NgModule } from "@angular/core";
import { Router, RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "@auth0/auth0-angular";
import { ArticleViewComponent } from "./article-view/article-view.component";
import { AuthButtonComponent } from "./auth-button/auth-button.component";
import { GraphComponent } from "./graph/graph.component";
import { JobListComponent } from "./job-list/job-list.component";
import { JobResultComponent } from "./job-result/job-result.component";
import { SearchPageComponent } from "./search-page/search-page.component";

const routes: Routes = [
    {path: '', component: AuthButtonComponent},
    {path: 'home', component: SearchPageComponent, canActivate: [AuthGuard]},
    {path: 'job-list', component: JobListComponent, canActivate: [AuthGuard]},
    {path: 'job-result', component: JobResultComponent, canActivate: [AuthGuard]},
    {path: 'article-view', component: ArticleViewComponent, canActivate: [AuthGuard]},
    {path: 'graph', component: GraphComponent}
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}