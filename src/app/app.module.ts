import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthConfig, AuthHttpInterceptor, AuthModule } from '@auth0/auth0-angular';

import { AppComponent } from './app.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { JobListComponent } from './job-list/job-list.component';
import { JobComponent } from './job/job.component';
import { AuthButtonComponent } from './auth-button/auth-button.component';
import { environment } from 'src/environments/environment';
import { JobResultComponent } from './job-result/job-result.component';
import { ArticleComponent } from './article/article.component';
import { LayoutModule } from '@angular/cdk/layout';
import { GraphComponent } from './graph/graph.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { ClusterButtonComponent } from './cluster-button/cluster-button.component';
import { ArticleViewComponent } from './article-view/article-view.component';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer';
import { ErrorMessageComponent } from './error-message/error-message.component';

const authConfig: AuthConfig = {
  domain: environment.oauth2.domain,
  clientId: environment.oauth2.clientId,

  // Request this scope at user authentication time
  scope: 'read:current_user',

  audience: environment.oauth2.audience,

  // Specify configuration for the interceptor
  httpInterceptor: {
    allowedList: [
      { uri: `${environment.apiUrl}/*` }
    ]
  }
}

@NgModule({
  declarations: [
    AppComponent,
    SearchPageComponent,
    JobListComponent,
    JobComponent,
    AuthButtonComponent,
    JobResultComponent,
    ArticleComponent,
    GraphComponent,
    ClusterButtonComponent,
    ArticleViewComponent,
    ErrorMessageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AuthModule.forRoot(authConfig),
    LayoutModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    }),
    PdfJsViewerModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptor, multi: true },
    Title
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
