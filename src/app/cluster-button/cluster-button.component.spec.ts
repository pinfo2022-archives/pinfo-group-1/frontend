import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClusterButtonComponent } from './cluster-button.component';

describe('ClusterButtonComponent', () => {
  let component: ClusterButtonComponent;
  let fixture: ComponentFixture<ClusterButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClusterButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClusterButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
