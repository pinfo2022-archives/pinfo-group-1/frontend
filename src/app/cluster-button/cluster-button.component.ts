import { Component, Input, OnInit } from '@angular/core';
import { GraphComponent } from '../graph/graph.component';

@Component({
  selector: 'app-cluster-button',
  templateUrl: './cluster-button.component.html',
  styleUrls: ['./cluster-button.component.scss']
})
export class ClusterButtonComponent implements OnInit {
  @Input() legend!: number
  @Input() all: boolean = false
  @Input() none: boolean = false
  @Input() index!: number

  constructor(public graph: GraphComponent) { }

  ngOnInit(): void {
  }

  onButtonClick() {
    this.graph.clicked[this.index] = true
    this.graph.makeGraph()
  }

  onRelease() {
    this.graph.clicked[this.index] = false
    this.graph.makeGraph()
  }

  onAllClick() {
    for (let i = 0; i < this.graph.clicked.length; i++) {
      this.graph.clicked[i] = true
    }
    this.graph.makeGraph()
  }

  onNoneClick() {
    for (let i = 0; i < this.graph.clicked.length; i++) {
      this.graph.clicked[i] = false
    }
    this.graph.makeGraph()
  }
}
