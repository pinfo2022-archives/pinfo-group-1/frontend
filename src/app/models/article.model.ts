export class Article{
    uuid!: string
    result_uuid!: string
    title!: string
    published!: string
    doi!: string
    pmcId!: string
    authors!: string 
    abstract!: string
    fulltext!: string
    url!: string
    journal!: string 
    labels!: string
    cluster!: number
    x!: number
    y!: number
}