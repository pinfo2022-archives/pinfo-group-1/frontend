export class RealJob {
    progress_fraction!: number
    status!: string
    timestamp!: number
    ucnf!: string
    uuid!: string
}