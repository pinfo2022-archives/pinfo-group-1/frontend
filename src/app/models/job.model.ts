export class Job{
    uuid!: string
    query!: string
    state!: string
    job_uuid!: string
    result_uuid!: string
}