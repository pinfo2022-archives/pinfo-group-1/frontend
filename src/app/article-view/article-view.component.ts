import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { DOCUMENT } from '@angular/common';
import { Component, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { Article } from '../models/article.model';
import { ArticleService } from '../services/article.service';
import { HttpClient } from "@angular/common/http";
import { map } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-article-view',
  templateUrl: './article-view.component.html',
  styleUrls: ['./article-view.component.scss']
})
export class ArticleViewComponent implements OnInit {
  article!: Article
  nickname?: string
  small: Boolean = false
  authors: string = ''
  pdfLoaded: boolean = false

  @ViewChild('pdfViewer') public pdfViewer: any

  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    this.auth.logout({ returnTo: document.location.origin })
  }

  constructor(@Inject(DOCUMENT) public document: Document, private http: HttpClient, private router: Router, public auth: AuthService, private articleService: ArticleService, private responsive: BreakpointObserver, private titleService: Title) { }

  ngOnInit(): void {
    window.scroll(0,0)
    this.article = this.articleService.articleToView
    this.titleService.setTitle('Researchado - '+this.article.title)
    for (let i = 0; i < this.article.authors.length; i++) {
      if (this.article.authors[i] == '[' || this.article.authors[i] == ']') {
        continue
      }
      else {
        this.authors += this.article.authors[i]
      }
    }
    this.auth.getUser().subscribe(user => {this.nickname = user?.nickname})
    this.responsive.observe(Breakpoints.XSmall).subscribe(result => {
      if (result.matches) {this.small = true} else {this.small = false}})
    let str = this.article.url.split('://')
    if (str[0] == 'http'){str[0]='https'}
    
    this.http.get(str[0]+'://'+str[1]+'.pdf', { responseType: 'blob' }).pipe(map((result: any) => {return result;}))
    .subscribe(
    	(res) => {
        	this.pdfViewer.pdfSrc = res;
          this.pdfLoaded = true
        	this.pdfViewer.refresh();
    	},
      _error => this.redo(str[0]+'://'+str[1]+'.pdf')
	  );
  }

  onReturnClick() {
    this.router.navigateByUrl('job-result')
  }

  redo(str: string) {
    const newUrl = str.split('/pdf/')
    const num = newUrl[1].split('.')
    const fnum = num[0]+'/'+num[0]+'.'+num[1].split('v')[0]+'.pdf'
    this.http.get(newUrl[0]+'/ftp/arxiv/papers/'+fnum , { responseType: 'blob' }).pipe(map((result: any) => {return result;}))
    .subscribe(
    	(res) => {
        	this.pdfViewer.pdfSrc = res;
          this.pdfLoaded = true
        	this.pdfViewer.refresh();
    	}
	  );
  }
}
