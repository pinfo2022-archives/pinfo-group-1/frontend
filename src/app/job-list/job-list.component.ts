import { DOCUMENT } from '@angular/common';
import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { Job } from '../models/job.model';
import { User } from '../models/user.model';
import { JobService } from '../services/job.service';
import { RequestService } from '../services/request.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.scss']
})
export class JobListComponent implements OnInit {
  search!: string
  finishJobs: Job[] = []
  runningJobs: Job[] = []
  user!: User
  nickname?: String
  small: Boolean = false

  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    this.auth.logout({ returnTo: document.location.origin })
  }

  constructor(@Inject(DOCUMENT) public document: Document, private router: Router, public auth: AuthService, private jobService: JobService, private requestService: RequestService, private responsive: BreakpointObserver, private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle('Researchado - Job List')
    this.updateJobs()
    this.auth.getUser().subscribe(user => {this.nickname = user?.nickname})
    this.responsive.observe(Breakpoints.XSmall).subscribe(result => {
      if (result.matches) {this.small = true} else {this.small = false}})
  }

  updateJobs() {
    this.finishJobs = []
    this.runningJobs = []
    this.jobService.getJobs().subscribe(data => {
      data.forEach((job) => {
        const container = job.job_uuid ? this.runningJobs : this.finishJobs
        container.push({
          uuid: job.uuid,
          query: job.query,
          state: job.job_uuid ? 'running' : 'finished',
          job_uuid: job.job_uuid,
          result_uuid: job.result_uuid
        })
      })
    })
  }

  onReturnClick() {
    this.router.navigateByUrl('home')
  }

}
