import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Article } from '../models/article.model';
import { ArticleService } from '../services/article.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  @Input() article!: Article
  authors: String = ""

  constructor(private articleService: ArticleService, private router: Router) { }

  ngOnInit(): void {
    for (let i = 0; i < this.article.authors.length; i++) {
      if (this.article.authors[i] == '[' || this.article.authors[i] == ']') {
        continue
      }
      else {
        this.authors += this.article.authors[i]
      }
    }
  }

  onArticleClick() {
    this.articleService.articleToView = this.article
    this.router.navigateByUrl('article-view')
  }
}
