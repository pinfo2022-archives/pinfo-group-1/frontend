import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Job } from '../models/job.model';
import { JobService } from '../services/job.service';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.scss']
})
export class JobComponent implements OnInit {
  @Input() job!: Job
  small: Boolean = false
  progress_fraction: number = 0
  status!: string

  constructor(private jobService: JobService, private router: Router, private responsive: BreakpointObserver) { }

  ngOnInit(): void {
    this.responsive.observe("(max-width: 959.98px)").subscribe(result => {
      if (result.matches) { this.small = true } else { this.small = false }
    })
    if (this.job.job_uuid) {
      this.jobService.getProSta(this.job.job_uuid).subscribe(data => {
        this.progress_fraction = data.progress_fraction
        this.status = data.status
      })
    }
  }

  onSearchMegaClick() {
    this.jobService.resID = this.job.result_uuid
    this.jobService.query = this.job.query
    this.router.navigateByUrl('job-result')
  }

}
