import { DOCUMENT } from '@angular/common';
import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { RequestService } from '../services/request.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Title } from '@angular/platform-browser';

declare var require: any;
const {XMLParser} = require('fast-xml-parser');

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit {
  numberOfArticle: number = 0
  previewClicked: boolean = false
  nickname?: String
  small: Boolean = false
  errorMessage: any[] = []

  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    this.auth.logout({ returnTo: document.location.origin })
  }

  constructor(@Inject(DOCUMENT) public document: Document, private router: Router, public auth: AuthService, private requestService: RequestService, private responsive: BreakpointObserver, private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle('Researchado - Home')
    this.auth.getUser().subscribe(user => {this.nickname = user?.nickname})
    this.responsive.observe(Breakpoints.XSmall)
      .subscribe(result => {
        if (result.matches) {
          this.small = true
        } else {
          this.small = false
        }
      })
  }

  onSearchClick(search: string) {
    this.errorMessage = []
    this.requestService.postRequest(search).subscribe(() => this.router.navigateByUrl('job-list'), (error) => {this.errorMessage = error.error.errors})
  }

  onPreviewClick(search: String) {
    this.previewClicked = true
    this.requestService.getPreview(search).subscribe(data => {
      const parser = new XMLParser()
      let res = parser.parse(data)
      this.numberOfArticle = res.feed["opensearch:totalResults"]
    })
  }

  onJobClick() {
    this.router.navigateByUrl('job-list')
  }
}
