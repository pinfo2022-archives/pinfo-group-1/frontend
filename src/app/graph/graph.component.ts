import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Article } from '../models/article.model';
import { ArticleService } from '../services/article.service';
import { JobService } from '../services/job.service';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit {
  color: String[] = ['#ff0000', '#00ff00', '#0000ff', '#800080', '#808000', '#008080', '#000000']
  options: any;
  clicked: boolean[] = []
  cluster: number[] = []
  sortedData: Article[][] = []

  constructor(private router: Router, private articleService: ArticleService, public jobService: JobService) {}

  ngOnInit(): void {
    for (let i = 0; i < this.jobService.articles.length; i++) {
      let index = this.cluster.indexOf(this.jobService.articles[i].cluster)
      if (index < 0) {
        this.cluster.push(this.jobService.articles[i].cluster)
        this.clicked.push(true)
        this.sortedData.push([this.jobService.articles[i]])
      } else {
        this.sortedData[index].push(this.jobService.articles[i])
      }
    }
    console.log(this.sortedData)
    console.log(this.cluster)
    this.makeGraph()
  }

  makeGraph(){
    const data_test = []

    for (let i = 0; i < this.cluster.length; i++) {
      if (this.clicked[i]){for (let j = 0; j < this.sortedData[i].length; j++) {
        let t = this.sortedData[i]
        data_test.push({name: t[j]["title"],
                        x: t[j]["x"],
                        y: t[j]["y"],
                        label: {show: false},
                        id: t[j]['uuid'],
                        itemStyle:{
                          color: this.color[i]
                        }
                      })
      }}
    }
    
    this.options = {
      title: {},
      tooltip: {
        trigger: 'item',
        formatter: '<strong>{b}</strong>'
      },
      animationDurationUpdate: 1500,
      animationEasingUpdate: 'quinticInOut',
      series: [
        {
          type: 'graph',
          layout: 'none',
          symbolSize: 10,
          roam: false,
          label: {
            show: false
          },
          data: data_test,
          links: [],
          animationDelay: (idx: number) => idx * 10
        }
      ]
    };
  }
  
  onChartEvent(event: any, type: string) {
    for (let i = 0; i < this.jobService.articles.length; i++) {
      if (this.jobService.articles[i].uuid == event.data.id){
        this.articleService.articleToView = this.jobService.articles[i]
      }
    }
    console.log('chart event:', type, event.data.id);
    this.router.navigateByUrl('article-view')
  }
}