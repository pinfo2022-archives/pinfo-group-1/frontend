import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { AuthService } from '@auth0/auth0-angular';
import { Router } from '@angular/router';
import { RequestService } from '../services/request.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-auth-button',
  templateUrl: './auth-button.component.html',
  styleUrls: ['./auth-button.component.scss']
})
export class AuthButtonComponent implements OnInit {

  constructor(@Inject(DOCUMENT) public document: Document, public auth: AuthService, private router: Router, private request: RequestService, private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle('Researchado')
    this.auth.isAuthenticated$.subscribe(data => {
      if (data) {
        // this.request.createUser()
        this.router.navigateByUrl('home')}})
  }

}
