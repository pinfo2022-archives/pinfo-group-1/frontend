import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService, User } from "@auth0/auth0-angular";
import { map } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})
export class RequestService {
    // user: User

    constructor(private auth: AuthService, private http: HttpClient) { }

    postRequest(query: String) {
        // this.auth.getUser().subscribe(user => {console.log(user)})
        return this.http.post(`${environment.apiUrl}/searches`, { "query": query })
    }

    createUser() {
        this.auth.getUser().pipe(map(user => { }))
    }

    getPreview(query: String){
        return this.http.get(`https://export.arxiv.org/api/query?search_query=${query}&start=0&max_results=0`, {responseType: 'text'})
    }
}