import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from "@auth0/auth0-angular";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { Article } from "../models/article.model";
import { Job } from "../models/job.model";
import { RealJob } from "../models/realJob.model";
import { User } from "../models/user.model";

@Injectable({
    providedIn: 'root'
})
export class JobService {
    Jobs!: Job[]
    resID!: string
    old: string = ''
    rowArticle: Article[] = []
    articles: Article[] = []
    query!: string

    constructor(public auth: AuthService, private http: HttpClient) { }

    getJobs(): Observable<Job[]> {
        return this.http.get<Job[]>(`${environment.apiUrl}/searches`)
    }

    getUser(user: string): Observable<User> {
        return this.http.get<User>(`${environment.apiUrl}/users/${user}`)
    }

    getRunnning(): Job[] {
        return this.Jobs
    }

    getFinish(): Job[] {
        return this.Jobs
    }

    getResults() {
        this.rowArticle = []
        this.articles = []
        this.http.get<Article[]>(`${environment.apiUrl}/results/${this.resID}/articles`).subscribe(art => {
            art.forEach((a) => {
                this.articles.push(a)
                this.rowArticle.push(a)
            })})
    }

    getProSta(uuid: string): Observable<RealJob> {
        return this.http.get<RealJob>(`${environment.apiUrl}/jobs/${uuid}`)
    }
}