import { Injectable } from "@angular/core";
import { Article } from "../models/article.model";

@Injectable({
    providedIn: 'root'
})
export class ArticleService {
    articleToView!: Article
    filterKey: string = ''
    filterAut: string = ''
    filtered: boolean = false

    constructor() {}
}