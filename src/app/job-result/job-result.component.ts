import { DOCUMENT } from '@angular/common';
import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { Article } from '../models/article.model';
import { JobService } from '../services/job.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Title } from '@angular/platform-browser';
import { ArticleService } from '../services/article.service';

@Component({
  selector: 'app-job-result',
  templateUrl: './job-result.component.html',
  styleUrls: ['./job-result.component.scss']
})
export class JobResultComponent implements OnInit {
  nickname?: string
  small: Boolean = false
  ssmall: boolean = false

  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    this.auth.logout({ returnTo: document.location.origin })
  }

  constructor(@Inject(DOCUMENT) public document: Document, private router: Router, public auth: AuthService, public jobService: JobService, private responsive: BreakpointObserver, private titleService: Title, public articleService: ArticleService) { }

  ngOnInit(): void {
    this.titleService.setTitle('Researchado - ' + this.jobService.query)
    if (this.jobService.resID != this.jobService.old) {
      this.jobService.old = this.jobService.resID
      this.init()
    }
    this.auth.getUser().subscribe(user => { this.nickname = user?.nickname })
    this.responsive.observe(Breakpoints.XSmall).subscribe(result => {
      if (result.matches) { this.small = true } else { this.small = false }
    })
    this.responsive.observe(Breakpoints.Small).subscribe(result => {
      if (result.matches) { this.ssmall = true } else { this.ssmall = false }
    })
  }

  init() {
    console.log('init')
    this.jobService.getResults()
  }

  onJobClick() {
    this.router.navigateByUrl('job-list')
  }

  onSearchClick(key: string, author: string) {
    let result: Article[] = []
    const keys = key.split(',').map(x => x.trim())
    const authors = author.split(',').map(x => x.trim())
    this.jobService.articles = this.jobService.articles
      .filter(article => authors
        .every(aut => article.authors.toUpperCase().includes(aut)))
      .filter(article => keys
        .every(k => article.title.toUpperCase().includes(k) || article.abstract.toUpperCase().includes(k)))

    keys
      .filter(x => !this.articleService.filterKey.includes(x))
      .forEach(x => { if (this.articleService.filterKey.length == 0) { this.articleService.filterKey += x } else { this.articleService.filterKey += ', ' + x } })
    authors
      .filter(x => !this.articleService.filterAut.includes(x))
      .forEach(x => { if (this.articleService.filterAut.length == 0) { this.articleService.filterAut += x } else { this.articleService.filterAut += ', ' + x } })

    this.articleService.filtered = true
    this.router.navigateByUrl('/graph', { skipLocationChange: true }).then(() => {
      this.router.navigate(['job-result']);
    });
  }

  onResetClick() {
    this.articleService.filtered = false
    this.jobService.articles = this.jobService.rowArticle
    this.articleService.filterAut = ''
    this.articleService.filterKey = ''
    this.router.navigateByUrl('/graph', { skipLocationChange: true }).then(() => {
      this.router.navigate(['job-result']);
    });
  }
}
