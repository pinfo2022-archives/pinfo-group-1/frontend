import { Environment } from "./environment.interface";

export const environment: Environment = {
  production: false,
  apiUrl: 'http://localhost:4200/api',
  oauth2: {
    domain: 'researchado-staging.eu.auth0.com',
    clientId: 'UkvvNVkBzoqKn4lOdkSbIca4GLU8VjyQ',
    audience: 'https://dev.researchado.ml'
  }
}