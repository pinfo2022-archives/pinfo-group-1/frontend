import { Environment } from "./environment.interface";

export const environment: Environment = {
  production: false,
  apiUrl: 'https://staging.researchado.ml/api',
  oauth2: {
    domain: 'researchado-staging.eu.auth0.com',
    clientId: '7K3HHKIynEM3dW7HZH1hZK7uCvur4W47',
    audience: 'https://dev.researchado.ml'
  }
}
