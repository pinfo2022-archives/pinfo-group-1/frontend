import { Environment } from "./environment.interface";

export const environment: Environment = {
  production: true,
  apiUrl: 'https://researchado.ml/api',
  oauth2: {
    domain: 'researchado.eu.auth0.com',
    clientId: 'XFBe2aAT0Pe60wJQj1zjwtoH6a04G8SG',
    audience: 'https://researchado.ml'
  }
}
