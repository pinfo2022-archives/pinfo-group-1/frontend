export interface Environment {
  production: boolean,
  oauth2: {
    domain: string,
    clientId: string,
    audience: string,
  },
  apiUrl: string
}